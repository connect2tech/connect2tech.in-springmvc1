package com.c2t.web.service;

import java.util.List;

import com.c2t.web.models.EmployeeVO;

public interface EmployeeManager
{
    public List<EmployeeVO> getAllEmployees();
}