package com.c2t.web.controller;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.c2t.annotation.basic.Employee;
import com.c2t.web.models.EmployeeVO;
import com.c2t.web.service.EmployeeManager;

@Controller
//@RequestMapping("/employee-module")
@RequestMapping("/admin")
@SessionAttributes("employee")
public class EmployeeController {
	@Autowired
	EmployeeManager manager;

	@RequestMapping(value = "/addNew", method = RequestMethod.GET)
	public String setupForm(Model model) {
		EmployeeVO employeeVO = new EmployeeVO();
		model.addAttribute("employee", employeeVO);
		return "addEmployee";
	}

	@RequestMapping(value = "/addNew", method = RequestMethod.POST)
	public String submitForm(@ModelAttribute("employee") EmployeeVO employeeVO, BindingResult result,
			SessionStatus status) {
		// Validation code start
		boolean error = false;

		System.out.println(employeeVO); // Verifying if information is same as
										// input by user

		if (employeeVO.getFirstName().isEmpty()) {
			result.rejectValue("firstName", "error.firstName");
			error = true;
		}

		if (employeeVO.getLastName().isEmpty()) {
			result.rejectValue("lastName", "error.lastName");
			error = true;
		}

		if (employeeVO.getEmail().isEmpty()) {
			result.rejectValue("email", "error.email");
			error = true;
		}

		if (error) {
			return "addEmployee";
		}
		// validation code ends

		// Store the employee information in database
		// manager.createNewRecord(employeeVO);

		// Mark Session Complete
		status.setComplete();
		return "redirect:addNew/success";
	}

	@RequestMapping(value = "/success", method = RequestMethod.GET)
	public String success(Model model) {
		return "addSuccess";
	}

	/*@RequestMapping(value = "/getAllEmployees", method = RequestMethod.GET)
	public String getAllEmployees(Model model) {*/

		/*SessionFactory sf = new AnnotationConfiguration().configure("com/c2t/annotation/basic/hibernate.cfg.xml")
				.buildSessionFactory();

		Session session = sf.openSession();

		Query q = session.createQuery("from Employee");
		List<Employee> list = q.list();

		for (int i = 0; i < list.size(); i++) {

			Employee e = list.get(i);
			System.out.println(e.getFirstname());
			System.out.println(e.getLastname());
			System.out.println("-------------------");

		}*/
		
		
/*
		model.addAttribute("employees", manager.getAllEmployees());
		return "employeesListDisplay";
	}*/
	
	@RequestMapping(value = "/getAllEmployees", method = RequestMethod.GET)
	public String getAllEmployees(Model model) {
	
		List<EmployeeVO> employees = new ArrayList<EmployeeVO>();

		EmployeeVO vo1 = new EmployeeVO();
		vo1.setId(1);
		vo1.setFirstName("Lokesh");
		vo1.setLastName("Gupta");
		employees.add(vo1);

		EmployeeVO vo2 = new EmployeeVO();
		vo2.setId(2);
		vo2.setFirstName("Raj");
		vo2.setLastName("Kishore");
		employees.add(vo2);
		
		

		model.addAttribute("employees", employees);
		return "employeesListDisplay";
	}
}