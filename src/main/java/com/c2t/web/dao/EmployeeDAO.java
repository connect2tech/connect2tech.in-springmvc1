package com.c2t.web.dao;

import java.util.List;

import com.c2t.web.models.EmployeeVO;

public interface EmployeeDAO {
	public List<EmployeeVO> getAllEmployees();
}
