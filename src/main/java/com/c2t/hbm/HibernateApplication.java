package com.c2t.hbm;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;

public class HibernateApplication {
	public static void main(String[] args) {
		SessionFactory sf = new AnnotationConfiguration().configure("").buildSessionFactory();
	}
}
